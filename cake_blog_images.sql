-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 24, 2017 at 10:05 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cake_blog_images`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `body` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `image_id`, `user_id`, `body`, `created`, `modified`) VALUES
(1, 1, 2, 'hahaha', '2017-04-24 04:19:08', '2017-04-24 04:19:08'),
(2, 2, 2, 'OMG!', '2017-04-24 04:30:01', '2017-04-24 04:30:01'),
(3, 2, 1, 'Great! I want one!', '2017-04-24 04:30:20', '2017-04-24 04:30:20'),
(4, 1, 1, 'she\'s genius!\n', '2017-04-24 04:31:00', '2017-04-24 04:31:00'),
(5, 11, 1, 'hahaha', '2017-04-24 07:07:58', '2017-04-24 07:07:58');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `url` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `user_id`, `title`, `description`, `url`, `created`, `modified`) VALUES
(11, 1, '', '', 'upload/images/13938221_10154341636776788_1774079369819258146_o.jpg', '2017-04-24 07:07:52', '2017-04-24 07:07:52'),
(12, 1, '', '', 'upload/images/13938221_10154341636776788_1774079369819258146_o.jpg', '2017-04-24 07:08:05', '2017-04-24 07:08:05'),
(13, 1, '', '', 'upload/images/29838_logo_0_552443.png', '2017-04-24 07:09:35', '2017-04-24 07:09:35');

-- --------------------------------------------------------

--
-- Table structure for table `phinxlog`
--

CREATE TABLE `phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phinxlog`
--

INSERT INTO `phinxlog` (`version`, `migration_name`, `start_time`, `end_time`, `breakpoint`) VALUES
(20170424024650, 'CreateUsers', '2017-04-23 19:52:15', '2017-04-23 19:52:15', 0),
(20170424032335, 'CreateImages', '2017-04-23 20:23:39', '2017-04-23 20:23:39', 0),
(20170424033326, 'CreateComments', '2017-04-23 20:33:32', '2017-04-23 20:33:33', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(20) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `role`, `created`, `modified`) VALUES
(1, 'huuvan20@gmail.com', '$2y$10$1R8fDdWxZKD1ssHMGWUktOJJGgZUmVcAkSqFls01PD4uMLP5Jaopi', 'admin', '2017-04-24 03:20:42', '2017-04-24 03:20:42'),
(2, 'huuvan19@gmail.com', '$2y$10$wdVTgr0Yt3S6Dx0gZJPbYuW3QyQsAjxFqxNadBZY8i8Fd7K4sMosW', 'member', '2017-04-24 03:21:26', '2017-04-24 07:10:57'),
(4, 'huuvan18@gmail.com', '$2y$10$91PVa5cJwpvGGFsT4TmoxeWggI3S9nTN222bNNnBsh5lBlZ93vqWO', 'member', '2017-04-24 07:10:38', '2017-04-24 07:10:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phinxlog`
--
ALTER TABLE `phinxlog`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
