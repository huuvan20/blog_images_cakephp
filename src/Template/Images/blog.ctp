
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Add an image post'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('My gallery'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Logout'), ['controller' => 'Users', 'action' => 'logout']) ?></li>
    </ul>
</nav>
<div class="images index large-9 medium-8 columns content">
    <?php foreach( $images as $image ) { ?>
        <div class="article">
            <div class="image-container">
                <h2><?= $image['title'] ?></h2>
                <img src="<?= $this->Url->build('/') . $image['url'] ?>" style="width: 300px;">
                <p><?= $image['description'] ?></p>
            </div>
            <div class="comments">
                <h3>Comments</h3>
                <?php foreach( $image['comments'] as $comment ) { ?>
                <div class="comment">
                    <strong><?= $comment['user']['email'] ?></strong>&nbsp&nbsp
                    <small><?= $comment['created'] ?></small>
                    <p><?= $comment['body'] ?></p>
                </div>
                <?php } ?>
            </div>
            <div class="comment-adding">
            <?php 
                $sessions = $this->request->session();
                echo $this->Form->create( null, ['class' => 'comment-form']);
                    echo $this->Form->hidden( 'userId', [
                        'value' => $sessions->read('Auth.User.id'),
                        'class' => 'user-id',
                    ]);
                    echo $this->Form->hidden( 'imageId', [
                        'value' => $image['id'],
                        'class' => 'image-id'
                    ]);
                    echo $this->Form->textarea( 'commentBody', [
                        'rows' => '3', 'cols' => '3',
                        'class' => 'comment-body'
                    ]);
                    echo $this->Form->submit('Submit');
                echo $this->Form->end();
            ?>
            </div>
        </div>
    <?php } ?>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>

<!--JS here-->
<?php $this->Html->scriptStart(['block' => true]); ?>
    $('.comment-form').submit(function( event ) {
        event.preventDefault();
        var commentsContainer = $(this).parent('div').prev('.comments');
        var userId      = $(this).children('.user-id');
        var imageId     = $(this).children('.image-id');
        var commentBody = $(this).children('.comment-body');
        var request = $.ajax({
            method      : "POST",
            url         : "<?= $this->Url->build(['action'=>'handleAjax']) ?>",
            dataType    : "JSON",
            data        : {
                name        : "addComment",
                user_id     : userId.val(),
                image_id    : imageId.val(),
                body        : commentBody.val()
            }
        });
        request.done(function( response ) {
            var comment = $("<div class='comment'>");
            comment.append( $("<strong>" + response.user.email + "&nbsp&nbsp</strong>") );
            comment.append( $("<small>new comment</small>") );
            comment.append( $("<p>" + response.body + "</p>") );
            comment.appendTo( commentsContainer );
            commentBody.val('');
        });
        request.fail(function( response ) {
            console.log(response);
        });
    });
<?php $this->Html->scriptEnd(); ?>

