
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Dashboard'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Edit User') ?></legend>
        <?php
            $sessions = $this->request->session();
            echo $this->Form->control('email');
            echo $this->Form->control('password');
            if ( $sessions->read('Auth.User.role') == 'member' ) {
                $options = ['member' => 'Member'];
            } else {
                $options = ['admin' => 'Admin', 'member' => 'Member'];
            }
            echo $this->Form->control('role', [
                'options' => $options
            ]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
