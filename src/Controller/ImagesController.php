<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Images Controller
 *
 * @property \App\Model\Table\ImagesTable $Images
 */
class ImagesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Comments');
    }

    public function isAuthorized($user)
    {
        $action = $this->request->getParam('action');
        // The add and index actions are always allowed.
        if (in_array($action, ['blog', 'add', 'index', 'handleAjax'])) {
            return true;
        }

        $role = $this->Auth->user('role');
        if (in_array($role, ['admin'])) {
            return true;
        }

        // Users could only view, edit their own <images></images>
        $passedId = $this->request->getParam('pass.0');
        $user = $this->Images->find()->select('user_id')->where(['id' => $passedId])->toArray();
        if ( $user[0]->user_id == $this->Auth->user('id') ) {
            return true;
        }

        return parent::isAuthorized($user);
    }

    public function handleAjax()
    {  
        if ( $this->request->is('ajax') ) {
            if ( $this->request->getData('name') == 'addComment' ) {
                $comment = $this->Comments->newEntity();
                $comment = $this->Comments->patchEntity($comment, $this->request->getData());
                if ($this->Comments->save($comment)) {
                    $justSavedComment = $this->Comments->get($comment->id, ['contain' => 'Users']);
                    echo json_encode( $justSavedComment );
                    die();
                } else {
                    echo json_encode(['isSaved' => false]);
                    die();
                }
            }
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $query = $this->Images->find()->where(['user_id' => $this->Auth->user('id')]);
        $images = $this->paginate($query);

        $this->set(compact('images'));
        $this->set('_serialize', ['images']);
    }

    /**
     * View method
     *
     * @param string|null $id Image id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $image = $this->Images->get($id, [
            'contain' => ['Users', 'Comments'=>['Users']]
        ]);

        $this->set('image', $image);
        $this->set('_serialize', ['image']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {     
        $image = $this->Images->newEntity();
        if ($this->request->is('post')) {
            $image = $this->Images->patchEntity($image, $this->request->getData());
            $image->user_id = $this->Auth->user('id');
             if ( in_array($this->request->data['submittedImage']['type'], ['image/jpeg', 'image/jpg', 'image/png']) ) {
                $image->url = $this->_saveImageAndReturnUrl( $this->request->data['submittedImage'] );
                if ($this->Images->save($image)) {
                    $this->Flash->success(__('The image has been saved.'));

                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('The image could not be saved. Please, try again.'));
             } else {
                $this->Flash->error(__('Must be a JPG or a PNG.'));
             }        
        }
        $this->set(compact('image'));
    
    }

    private function _saveImageAndReturnUrl( $submittedImage ) {
        
        //Check folder was created
        if (!is_dir( WWW_ROOT.DS."upload".DS."images" ))
        {
            mkdir( WWW_ROOT.DS."upload", 0777 );
            mkdir( WWW_ROOT.DS."upload".DS."images", 0777 );
        }
        $targetDir = WWW_ROOT.DS."upload".DS."images";
        $imageName = $submittedImage['name'];
        if ( move_uploaded_file($submittedImage['tmp_name'], $targetDir . DS . $imageName) ) 
        {
            $targetImage = 'upload/images/' . $imageName;
        }
        return $targetImage;
        
    }

    /**
     * Edit method
     *
     * @param string|null $id Image id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $image = $this->Images->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $image = $this->Images->patchEntity($image, $this->request->getData());
            if ($this->Images->save($image)) {
                $this->Flash->success(__('The image has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The image could not be saved. Please, try again.'));
        }
        $users = $this->Images->Users->find('list', ['limit' => 200]);
        $this->set(compact('image', 'users'));
        $this->set('_serialize', ['image']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Image id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $image = $this->Images->get($id);
        if ($this->Images->delete($image)) {
            $this->Flash->success(__('The image has been deleted.'));
        } else {
            $this->Flash->error(__('The image could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function blog() {
        $this->paginate = [
            'contain' => ['Users',
                         'Comments' => ['Users']],
            'order' => [
                'created' => 'desc'
            ]
        ];
        $images = $this->paginate($this->Images);

        $this->set(compact('images'));
        $this->set('_serialize', ['images']);
    }
}
